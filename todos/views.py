from django.shortcuts import render
from rest_framework import viewsets
from todos.models import Todolist
from todos.serializers import TodolistSerializer


class TodolistViewSet(viewsets.ModelViewSet):
    queryset = Todolist.objects.all()
    serializer_class = TodolistSerializer