from django.db import models
from django.contrib.postgres.fields import JSONField


class Todolist(models.Model):
    name = models.CharField(max_length=75, null=True)
    list = JSONField(default=dict(), null=True)
