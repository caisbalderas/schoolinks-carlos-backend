from rest_framework import serializers
from todos.models import Todolist

class TodolistSerializer(serializers.ModelSerializer):

    class Meta:
        model = Todolist
        fields = ('name', 'list')