"""schoolinks URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from rest_framework.response import Response
from rest_framework import routers
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
from todos.views import TodolistViewSet


@api_view(('GET',))
def api_root(request, format=None):
    return Response({
        'todolists': reverse('todolist-list', request=request, format=format),
    })

router = routers.SimpleRouter()
router.register(r'todolists', TodolistViewSet)

urlpatterns = [
    url(r'^$', api_root, name='api-root'),
    url(r'^', include(router.urls)),
]
